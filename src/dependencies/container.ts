import { IPredictionServiceName, PredictionService } from '@/business/PredictionService'
import { Predictions, IPredictionsName } from '@/business/Predictions';

class Container {
    public factories: { [id: string]: (c: Container) => any } = {}

    register(name: string, factory: (c: Container) => any): void {
        this.factories[name] = factory
    }

    resolve(name: string): any {
        if (!this.factories[name]) {
            throw new Error(`No resolver has been registered for dependency [${name}].`)
        }

        return this.factories[name](this)
    }
}

const container = new Container()

container.register(IPredictionsName, (_c) => Predictions)
container.register(IPredictionServiceName, (c) => new PredictionService(c.resolve(IPredictionsName)))

const proxy = new Proxy(
    container.factories,
    {
        get: (_obj: {}, prop: string) => container.resolve(prop)
    }
)

export default proxy