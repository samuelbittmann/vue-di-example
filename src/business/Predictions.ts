export const IPredictionsName = 'IPredictions'

export type IPredictions = string[]

export const Predictions = [
    'Maybe later',
    'Better not',
    'Why not',
    'What could possibly go wrong',
    'Only if you\'re courageous',
    'Absolutely not',
    'Definitively yes'
]