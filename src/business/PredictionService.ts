import { IPredictions } from './Predictions';

export default interface IPredictionService {
    getPrediction(): string
}

export const IPredictionServiceName = "IPredictionService"
export const IPredictionServiceSymbol: unique symbol = Symbol("IPredictionService")

export class PredictionService implements IPredictionService {
    private readonly predictions: IPredictions;

    public constructor(predictions: IPredictions) {
        this.predictions = predictions
    }

    public getPrediction(): string {
        return this.predictions[Math.floor(Math.random() * this.predictions.length)]
    }
}